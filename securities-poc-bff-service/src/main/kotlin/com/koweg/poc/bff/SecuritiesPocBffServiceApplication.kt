package com.koweg.poc.bff

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@EnableEurekaClient
@SpringBootApplication
class SecuritiesPocBffServiceApplication

fun main(args: Array<String>) {
	runApplication<SecuritiesPocBffServiceApplication>(*args)
}
