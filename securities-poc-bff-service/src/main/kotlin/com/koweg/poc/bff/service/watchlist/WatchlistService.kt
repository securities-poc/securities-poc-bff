package com.koweg.poc.bff.service.watchlist

import java.math.BigDecimal
import java.time.LocalDateTime

interface WatchlistService {
    suspend fun getWatchList(watchlistName: String): WatchList
}

data class WatchList (val watchlistName: String,
                      val securities: List<Security>)

data class Security(val isin: String,
                    var wkn: String,
                    val symbol: String,
                    val description: String,
                    val marketRate: BigDecimal,
                    val entryPrice: BigDecimal,
                    val entryDate: LocalDateTime,
                    val quantity: Int,
                    val currency: String)
