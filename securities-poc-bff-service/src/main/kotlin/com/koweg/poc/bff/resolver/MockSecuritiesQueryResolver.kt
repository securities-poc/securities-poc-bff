package com.koweg.poc.bff.resolver

import com.koweg.poc.bff.api.Query
import com.koweg.poc.bff.api.model.*
import graphql.kickstart.tools.GraphQLQueryResolver
import org.joda.time.DateTime
import org.springframework.stereotype.Component
import java.time.DateTimeException
import java.time.LocalDateTime


@Component
class   MockSecuritiesQueryResolver: Query, GraphQLQueryResolver {

    companion object MockPortfolioRepository{
        var portfolios = mutableMapOf<String, PortfolioDTO>(
                "ING" to PortfolioDTO("ING Portfolio", CurrencyDTO.GBP,
                        listOf(PositionDTO("US5949181045", "MSFT","Microsoft",170.25,100,"EUR"),
                                PositionDTO("US92826C8394", "V","Visa Inc",185.25,225,"USD")),
                        DateTime.now().toDate()),
                "ISA" to PortfolioDTO("ISA Portfolio", CurrencyDTO.GBP,
                        listOf(PositionDTO("US00206R1023", "T","AT&T Inc",29.25,205,"USD"),
                                PositionDTO("NL0000235190", "AIR","AIRBUS SE",56.29,84,"EUR")),
                        DateTime.now().toDate())
                )

        fun getPorfolio(portfolioId: String): PortfolioDTO? {
            return portfolios.get(portfolioId)
        }
    }

    override fun getExchangeRates(currency: CurrencyDTO?): CurrencyQuoteDTO {
        TODO("Not yet implemented")
    }

    override fun getAllStockAlerts(): MutableCollection<StockAlertDTO> {
        TODO("Not yet implemented")
    }

    override fun getWatchlist(): MutableCollection<WatchlistDTO> {
        TODO("Not yet implemented")
    }

    override fun getAllPortfolios(): MutableCollection<PortfolioDTO> {
        return MockPortfolioRepository.portfolios.values
    }

}
