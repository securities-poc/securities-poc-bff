<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>com.koweg.poc</groupId>
    <artifactId>securities-poc-bff</artifactId>
    <version>0.0.1-SNAPSHOT</version>
  </parent>

	<artifactId>securities-poc-bff-service</artifactId>
	<name>securities-poc-bff-service</name>
	<description>Securities BFF</description>

	<properties>
		<graphql-kickstart.version>7.0.1</graphql-kickstart.version>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>


	<build>
    <finalName>${project.artifactId}</finalName>
		<sourceDirectory>${project.basedir}/src/main/kotlin</sourceDirectory>
		<testSourceDirectory>${project.basedir}/src/test/kotlin</testSourceDirectory>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.jetbrains.kotlin</groupId>
				<artifactId>kotlin-maven-plugin</artifactId>
				<configuration>
					<args>
						<arg>-Xjsr305=strict</arg>
					</args>
					<compilerPlugins>
						<plugin>spring</plugin>
					</compilerPlugins>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>org.jetbrains.kotlin</groupId>
						<artifactId>kotlin-maven-allopen</artifactId>
						<version>${kotlin.version}</version>
					</dependency>
				</dependencies>
			</plugin>
			<!-- =============== Jib plugin =============== -->
			<plugin>
				<groupId>com.google.cloud.tools</groupId>
				<artifactId>jib-maven-plugin</artifactId>
				<version>2.1.0</version>
				<configuration>
					<from>
						<image>azul/zulu-openjdk-alpine:11.0.5-jre</image>
					</from>
					<to>
						<image>${project.artifactId}</image>
						<tags>
							<tag>latest</tag>
						</tags>
					</to>
					<container>
						<creationTime>USE_CURRENT_TIMESTAMP</creationTime>
					</container>
				</configuration>
			</plugin>
			<!-- =============== GraphQL Codegen Maven plugin =============== -->
			<plugin>
				<groupId>io.github.kobylynskyi</groupId>
				<artifactId>graphql-codegen-maven-plugin</artifactId>
				<version>1.7.2</version>
				<executions>
					<execution>
						<goals>
							<goal>generate</goal>
						</goals>
						<configuration>
							<graphqlSchemaPaths>
								<graphqlSchemaPath>${project.basedir}/src/main/resources/graphql/schema.graphqls</graphqlSchemaPath>
							</graphqlSchemaPaths>
							<outputDir>${project.build.directory}/generated-sources/graphql</outputDir>
							<apiPackageName>com.koweg.poc.bff.api</apiPackageName>
							<modelPackageName>com.koweg.poc.bff.api.model</modelPackageName>
							<customTypesMapping>
								<DateTime>java.util.Date</DateTime>
							</customTypesMapping>
							<modelNameSuffix>DTO</modelNameSuffix>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>integration-build</id>
			<activation>
				<activeByDefault>false</activeByDefault>
				<property>
					<name>spring.profiles.active</name>
					<value>int</value>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>pl.project13.maven</groupId>
						<artifactId>git-commit-id-plugin</artifactId>
						<version>4.0.0</version>
						<executions>
							<execution>
								<goals>
									<goal>revision</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<generateGitPropertiesFile>true</generateGitPropertiesFile>
							<generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties</generateGitPropertiesFilename>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>local-build</id>
			<activation>
				<activeByDefault>false</activeByDefault>
				<property>
					<name>spring.profiles.active</name>
					<value>local</value>
				</property>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>pl.project13.maven</groupId>
						<artifactId>git-commit-id-plugin</artifactId>
						<version>4.0.0</version>
						<executions>
							<execution>
								<goals>
									<goal>revision</goal>
								</goals>
							</execution>
						</executions>
						<configuration>
							<generateGitPropertiesFile>true</generateGitPropertiesFile>
							<generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties</generateGitPropertiesFilename>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-amqp</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-cache</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-mongodb-reactive</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-redis</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<!--
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-webflux</artifactId>
		</dependency>
		-->

		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>2.10.6</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-websocket</artifactId>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.module</groupId>
			<artifactId>jackson-module-kotlin</artifactId>
		</dependency>
		<dependency>
			<groupId>io.projectreactor.kotlin</groupId>
			<artifactId>reactor-kotlin-extensions</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jetbrains.kotlin</groupId>
			<artifactId>kotlin-reflect</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jetbrains.kotlin</groupId>
			<artifactId>kotlin-stdlib-jdk8</artifactId>
		</dependency>
		<dependency>
			<groupId>org.jetbrains.kotlinx</groupId>
			<artifactId>kotlinx-coroutines-reactor</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-stream-binder-rabbit</artifactId>
		</dependency>

		<dependency>
			<groupId>org.postgresql</groupId>
			<artifactId>postgresql</artifactId>
			<scope>runtime</scope>
		</dependency>



		<!-- COMMON CLOUD SERVICES -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-bus</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>




		<!--  GraphQL -->
		<dependency>
			<groupId>com.graphql-java-kickstart</groupId>
			<artifactId>graphql-spring-boot-starter</artifactId>
			<version>${graphql-kickstart.version}</version>
		</dependency>
		<dependency>
			<groupId>com.graphql-java-kickstart</groupId>
			<artifactId>graphiql-spring-boot-starter</artifactId>
			<version>${graphql-kickstart.version}</version>
		</dependency>

		<!--
		<dependency>
			<groupId>com.graphql-java-kickstart</groupId>
			<artifactId>graphql-kickstart-spring-boot-starter-webflux</artifactId>
			<version>${graphql-kickstart.version}</version>
		</dependency>
		-->

		<dependency>
			<groupId>com.graphql-java-kickstart</groupId>
			<artifactId>graphql-kickstart-spring-boot-starter-tools</artifactId>
			<version>${graphql-kickstart.version}</version>
		</dependency>

		<!-- GraphQL end -->








		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.junit.vintage</groupId>
					<artifactId>junit-vintage-engine</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>de.flapdoodle.embed</groupId>
			<artifactId>de.flapdoodle.embed.mongo</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>io.projectreactor</groupId>
			<artifactId>reactor-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.amqp</groupId>
			<artifactId>spring-rabbit-test</artifactId>
			<scope>test</scope>
		</dependency>
		<!--  GraphQL Testing -->
		<dependency>
			<groupId>com.graphql-java-kickstart</groupId>
			<artifactId>graphql-spring-boot-starter-test</artifactId>
			<version>${graphql-kickstart.version}</version>
			<scope>test</scope>
		</dependency>
	</dependencies>
</project>
